var $messages = $('.messages-content'),
    d, h, m,
    i = 0;

var obj = {
	"query":null,
	"state":null,
	"context":{},
	"intent":null
}

$(window).load(function() {
  $messages.mCustomScrollbar();
});

function updateScrollbar() {
	$messages.mCustomScrollbar("update").mCustomScrollbar('scrollTo', 'bottom', {
		scrollInertia: 10,
		timeout: 0
	});
}

function setDate(){
	d = new Date()
	if (m != d.getMinutes()) {
		m = d.getMinutes();
		$('<div class="timestamp">' + d.getHours() + ':' + m + '</div>').appendTo($('.message:last'));
	}
}

function insertMessage() {
	msg = $('.message-input').val();
	if ($.trim(msg) == '') {
		return false;
	}
	$('<div class="message message-personal">' + msg + '</div>').appendTo($('.mCSB_container')).addClass('new');
	

	$('.message-input').val(null);
	updateScrollbar();
	interact(msg);
}

$('.message-submit').click(function() {
	insertMessage();
});

$(window).on('keydown', function(e) {
	if (e.which == 13) {
		insertMessage();
		return false;
	}
})

function interact(message){
	
	obj['query'] = message;
    // loading message
    $('<div class="message loading new"><figure class="avatar"><img src="/static/res/botim.png" /></figure><span></span></div>').appendTo($('.mCSB_container'));

    // make a POST request [ajax call]
    $.post('http://localhost:8080/message', JSON.stringify(obj)).done(function(reply) {
		
		// Message Received
		// 	remove loading meassage
		$('.message.loading').remove();
		obj['state'] = reply['state'];
		obj['context'] = reply['context'];
		obj['intent'] = reply['intent'];
		$('<div class="message new"><figure class="avatar"><img src="/static/res/botim.png" /></figure>' + reply['text'] + '</div>').appendTo($('.mCSB_container')).addClass('new');
		
		setDate();
		updateScrollbar();

    }).fail(function() {
    	alert('error calling function');
    });
}
